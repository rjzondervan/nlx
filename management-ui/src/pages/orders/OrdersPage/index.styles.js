// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const Centered = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 10rem auto;
  max-width: 32rem;
  text-align: center;
`

export const StyledActionsBar = styled.div`
  display: flex;
  justify-content: flex-end;
`
